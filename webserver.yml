# SPDX-FileCopyrightText: 2018-2021 Michael Pöhn <michael@poehn.at>
# SPDX-License-Identifier: GPL-3.0-or-later
---
- name: "deploy status page"
  hosts: all

  tasks:
    - name: "apt: install unattended-upgrades"
      ansible.builtin.apt:
        install_recommends: no
        name: unattended-upgrades

    - name: "copy: enable unattended-upgrades"
      ansible.builtin.copy:
        content: |
          APT::Periodic::Enable "1";
          APT::Periodic::Download-Upgradeable-Packages "1";
          APT::Periodic::Unattended-Upgrade "1";
          APT::Periodic::Update-Package-Lists "1";
        dest: /etc/apt/apt.conf.d/20auto-upgrades
        mode: 0644
        owner: root
        group: root

    - name: "systemd: enable & unmask apt-daily services"
      ansible.builtin.systemd:
        name: "{{ item }}"
        enabled: true
        masked: false
      with_items:
        - apt-daily
        - apt-daily-upgrade

    - name: "systemd: enable apt-daily timers"
      ansible.builtin.systemd:
        name: "{{ item }}"
        state: started
        enabled: true
      with_items:
        - apt-daily.timer
        - apt-daily-upgrade.timer

    #######################################################
    # webserver setup for {{ domain }}

    - name: "apt: install geoip packages for nginx"
      ansible.builtin.apt:
        install_recommends: no
        name:
          - libnginx-mod-http-geoip
          - geoip-database

    - name: "include_role: get lets encrypt certificate via certbot"
      ansible.builtin.include_role:
        name: ansible-debian9-certbot
      vars:
        email: 'team@f-droid.org'
        update_via_nginx: yes

    - name: "nginx : make sure nginx is installed"
      ansible.builtin.package:
        name: nginx
        state: present

    - name: "check for dhparam"
      ansible.builtin.stat:
        path: /etc/ssl/dhparams.pem
      register: dhparams

    - name: "generate new dhparams"
      ansible.builtin.shell: "openssl dhparam -out /etc/ssl/dhparams.pem 4096 && chmod 600 /etc/ssl/dhparams.pem"
      when: not dhparams.stat.exists
      changed_when: true

    - name: "nginx: deploy site for '{{ domain }}'"
      ansible.builtin.template:
        src: nginx.conf.j2
        dest: "/etc/nginx/sites-available/{{ domain }}"
        owner: root
        group: root
        mode: 0644
      vars:
        ssl_cert_path: "/etc/letsencrypt/live/{{ domain }}/fullchain.pem"
        ssl_key_path: "/etc/letsencrypt/live/{{ domain }}/privkey.pem"
      notify: reload nginx

    - name: "nginx: enable site for '{{ domain }}'"
      ansible.builtin.file:
        src: "/etc/nginx/sites-available/{{ domain }}"
        dest: "/etc/nginx/sites-enabled/{{ domain }}"
        state: "link"
      notify: reload nginx

    - name: "blockinfile: nginx.conf customization"
      ansible.builtin.blockinfile:
        path: "/etc/nginx/nginx.conf"
        insertafter: 'http {'
        marker: "## {mark} ANSIBLE MANAGED BLOCK"
        content: |
            geoip_country /usr/share/GeoIP/GeoIPv6.dat;
            log_format privacy '0.0.0.0 - $remote_user [$time_local] "$request" $status $body_bytes_sent "$http_referer" "-" $geoip_country_code';

    - name: "replace: configure privacy freindly nginx access logging"
      ansible.builtin.replace:
        path: "/etc/nginx/nginx.conf"
        regexp: "access_log .*;"
        replace: "access_log /var/log/nginx/access.log privacy;"

    - name: "replace: turn nginx error logging off"
      ansible.builtin.replace:
        path: "/etc/nginx/nginx.conf"
        regexp: "error_log .*;"
        replace: "error_log off;"

  handlers:
    - name: reload nginx
      ansible.builtin.service:
        name: nginx
        state: reloaded
